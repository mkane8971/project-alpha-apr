from django.forms import ModelForm, DateTimeInput
from tasks.models import Task


class CreateTask(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            'start_date': DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
            'due_date': DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(CreateTask, self).__init__(*args, **kwargs)
        if user is not None:
            self.fields['project'].queryset = user.projects.all()
        self.fields['start_date'].input_formats = ('%Y-%m-%dT%H:%M',)
        self.fields['due_date'].input_formats = ('%Y-%m-%dT%H:%M',)
