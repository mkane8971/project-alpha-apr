from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST, user=request.user)
        if form.is_valid():
            form.save()
        return redirect("list_projects")
    else:
        form = CreateTask(user=request.user)
        context = {"form": form}
        return render(request, "tasks/create_task.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    task_list = []
    for task in tasks:
        task_list.append(
            {
                "name": task.name,
                "start_date": task.start_date,
                "due_date": task.due_date,
                "is_completed": task.is_completed,
            }
        )
    context = {
        "tasks": tasks
        }
    return render(request, "tasks/task_list.html", context)


@login_required
def edit_task(request, task_id):
    task = Task.objects.get(id=task_id)

    if request.method == "POST":
        form = CreateTask(request.POST, instance=task, user=request.user)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask(instance=task, user=request.user)

    context = {
        "form": form,
        "task_id": task_id
        }
    return render(request, "tasks/edit_task.html", context)
