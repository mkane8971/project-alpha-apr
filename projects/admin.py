from django.contrib import admin
from projects.models import Project, Company

# Register your models here.

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "email",
        "phone",
        "website",
        "description",
    ]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "owner",
        "company",
    ]
