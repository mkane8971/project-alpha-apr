from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class Company(models.Model):
    name= models.CharField(max_length=200)
    email= models.EmailField()
    phone= models.CharField(max_length=10)
    website= models.URLField(blank=True, null=True)
    description= models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )
    company= models.ForeignKey(
        Company,
        related_name="projects",
        on_delete= models.CASCADE,
        null=True,
    )
    def __str__(self):
        return self.name
