from django.urls import path
from projects.views import list_projects, show_project, create_project, search_projects_for_company

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path('search-projects/', search_projects_for_company, name='search_projects_for_company'),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
