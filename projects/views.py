from django.shortcuts import render, redirect
from projects.models import Project, Company
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject


# Create your views here.
@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "projects": project,
    }
    return render(request, "projects/home.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id, owner=request.user)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            owner = request.user
            project = form.save(False)
            project.owner = owner
            project.save()
        return redirect("list_projects")
    else:
        form = CreateProject
        context = {"form": form}
        return render(request, "projects/create_project.html", context)


def search_projects_for_company(request):
    query = request.GET.get('query')
    if query:
        company = Company.objects.filter(name__icontains=query).first()
        if company:
            projects = company.projects.all()
        else:
            projects = Project.objects.none()
    else:
        projects = Project.objects.none()
    context = {
        'projects': projects,
        'query': query
    }
    return render(request, 'projects/search_project.html', context)
